---
title: Choqok 1.2 released
date: 2011-11-01T13:46:11+00:00
author: Mehrdad
layout: post
categories:
  - news
---
I&#8217;m really proud to announce Choqok 1.2 release, code named &#8220;**Red Rose**&#8221; and I want to dedicate it to my beloved fiance, Golnaz.<!--more-->

<div id="attachment_309" style="width: 242px" class="wp-caption alignright">
  <a href="/uploads/choqok_1.2.png"><img class="size-medium wp-image-309" title="choqok_1.2" src="/uploads/choqok_1.2-232x400.png?fit=232%2C400" alt="Choqok new UI" srcset="/uploads/choqok_1.2.png?resize=232%2C400 232w, /uploads/choqok_1.2.png?resize=87%2C150 87w, /uploads/choqok_1.2.png?w=441 441w" sizes="(max-width: 232px) 100vw, 232px" data-recalc-dims="1" /></a>
  
  <p class="wp-caption-text">
    Choqok new UI
  </p>
</div>

### UI Design Changes

First change you will notice, is in user experience.

The idea came from my friend [Bardia Daneshvar](http://identi.ca/bardiax), and he was the one who implemented it. ( So, go and thank him for this change )

In previous UI we were using tabs to separate multiple accounts and again tabs were used inside accounts to separate timelines, but here there&#8217;s a difference between accounts and timelines representation in UI, and I liked this idea. (Also, We can always traverse between accounts via Ctrl+Tab in this design.)

&nbsp;

### StatusNet Changes

Another important change in Choqok 1.2 is some integrations with new changes on StatusNet, We now have a text saying **in reply to @mtux**, under posts that are reply to me. because StatusNet websites (including identica) are removing @mtux in dent texts by default.

Also when you click on reply to reply to someone, you can see a text like this under composer widget, and you&#8217;ll notice that this post will be a reply to someone.

[<img class="alignleft size-full wp-image-310" title="choqok_1.2_replying" src="/uploads/choqok_1.2_replying.png?fit=393%2C145" alt="" srcset="/uploads/choqok_1.2_replying.png?w=393 393w, /uploads/choqok_1.2_replying.png?resize=150%2C55 150w" sizes="(max-width: 393px) 100vw, 393px" data-recalc-dims="1" />](/uploads/choqok_1.2_replying.png)

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

### Global Menu

[<img class="alignright size-thumbnail wp-image-311" title="choqok_1.2_globalmenu" src="/uploads/choqok_1.2_globalmenu-200x200.png?resize=200%2C200" alt="" srcset="/uploads/choqok_1.2_globalmenu.png?resize=200%2C200 200w, /uploads/choqok_1.2_globalmenu.png?resize=64%2C64 64w" sizes="(max-width: 200px) 100vw, 200px" data-recalc-dims="1" />](/uploads/choqok_1.2_globalmenu.png)We want to have more space to do important things, reading/writing posts and communicating with friends. So, here we have an option to have more space in Choqok window, you can hide menubar and see a button on the top right of UI that will give you more important actions to do.

&nbsp;

### Twitter Users

We have two simple but I think important changes for you,

Twitter is shortening every link you enter in your tweet, even if it&#8217;s already shortened, So, Untiny plugin will expand those links twice to give you the actual link.

Also there was an annoying bug in Choqok at direct messages support, is fixed.

&nbsp;

### Other Changes

There are some more changes in Choqok that maybe you don&#8217;t notice at all, like:

  * A hyperlink on top of translated post to show original post.
  * is.gd statistics support.
  * Font customization option.
  * Better conversation view like new identica.
  * and much more bug fixes since previous release.

&nbsp;

### Download

You can download Choqok 1.2 source code [here](http://sourceforge.net/projects/choqok/files/Choqok/choqok-1.2.tar.bz2/download), and build it manually. or wait for binary packages on your distribution package manager.

Soon, there will be some solutions for popular distributions in [download page](http://choqok.gnufolks.org/download/).

&nbsp;

### Contribute

Just like before, you can [donate some money](http://choqok.gnufolks.org/about/contribute/) and help us on Choqok development and keep it up and going forward.