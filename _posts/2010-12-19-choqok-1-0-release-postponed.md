---
title: Choqok 1.0 release postponed
date: 2010-12-19T01:29:13+00:00
author: Mehrdad
layout: post
categories:
  - news
---
<img src="/uploads/hi64-app-choqok.png" alt="" align="left" data-recalc-dims="1" />We had announced the release of final Choqok 1.0 for 26-Dec, but things changed and we decided to postpone it to have a more stable and reliable release.

<div>
  <p>
    <!--more-->
  </p>
  
  <div>
    The reasons for this decision are:
  </div>
  
  <div>
    <ol>
      <li>
        our busyness since RC release that kept us from stabilization.
      </li>
      <li>
        And maybe more important reason is less feedbacks (specially bug reports) over latest RC1 release.
      </li>
    </ol>
    
    <div>
      So, <strong>The Choqok 1.0 will be released with KDE 4.6 at 26-Jan.</strong>
    </div>
  </div>
</div>

<div>
  You can help us by testing <a href="http://choqok.gnufolks.org/2010/12/choqok-1-0-rc1-released-huma/">the latest RC release</a>, and giving us feedback (specially on its problems).
</div>

<div>
  <span style="color: #ffffff;">.</span>
</div>

Have fun,
  
[Choqok](http://www.choqok.org/) dev team