---
title: News
date: 2010-07-11T16:36:22+00:00
author: Mehrdad
layout: page
---
<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{ post.title }}</a>
    </li>
  {% endfor %}
</ul>
