---
title: Choqok 1.3 released!
date: 2012-03-05T02:44:25+00:00
author: Mehrdad
layout: post
categories:
  - news
---
I am really happy to announce you another version of Choqok, Our lovely microblog client.
  
Unfortunately, life is going to make us busier everyday, But I&#8217;m trying my best to keep this app up and alive.

And once again, it&#8217;s near [Nowruz](http://en.wikipedia.org/wiki/Nowruz), Iranian new year. So, I dedicated this version to all of my people around the world, with an Iranian cultural code name and specially, Icon and splash screen for this release.

Here you are, Choqok version 1.3 code named &#8220;[Hajji Firuz](http://en.wikipedia.org/wiki/Hajji_Firuz)&#8220;.<!--more-->

<p style="text-align: center;">
  <img class="aligncenter  wp-image-337" title="Choqok 1.3 splash screen" src="/uploads/splash_screen3.png?resize=294%2C221" alt="" srcset="/uploads/splash_screen3.png?w=420 420w, /uploads/splash_screen3.png?resize=400%2C300 400w, /uploads/splash_screen3.png?resize=150%2C112 150w" sizes="(max-width: 294px) 100vw, 294px" data-recalc-dims="1" />
</p>

### Changes for this release:

[<img class=" wp-image-340 alignright" title="choqok-1.3" src="/uploads/choqok-1.3.png?resize=294%2C347" alt="" srcset="/uploads/choqok-1.3.png?w=409 409w, /uploads/choqok-1.3.png?resize=340%2C400 340w, /uploads/choqok-1.3.png?resize=127%2C150 127w" sizes="(max-width: 294px) 100vw, 294px" data-recalc-dims="1" />](/uploads/choqok-1.3.png)You can see most of changes for this release in this screen shot:

**Toolbar for Choqok**

We added toolbar for Choqok to make it easier to do somethings, that by default contains &#8220;Quick Post&#8221;, &#8220;Update Timelines&#8221; and &#8220;Upload Medium&#8221; and just like other toolbars in KDE, you can add/remove and customize it&#8217;s items.

**Search name over Icon**

Another change is that you can see first 4 characters of a search timeline name, over it&#8217;s Icon, so when you have more than one search timelines open, it will be quite easy to distinguish them from each other.

Also, there are some other minor improvements in UI of timeline bar.

**Highlight Post feature in Filtering**

In the screenshot, you see one of posts border is red and thicker, and that&#8217;s because our _Filter plugin_ has a new feature, You can set a filter to highlight some posts that are in your interest. Here I set a filter to highlight posts contains &#8220;choqok&#8221;.

**Twitter photo uploading**

Recently twitter added ability to upload images directly to its servers, just like StatusNet, and now we have support to upload images directly from Choqok, just like what we had for StatusNet. At the right side of composer text box, you&#8217;ll see the attach button. (Kudos to Atanas Gospodinov and Google Code In program)

**Translator plugin suspended**

Unfortunately, Google translate api changed to a paid service, and You cannot use it on Choqok anymore. So, We have suspended the plugin for now, until we find another solution (I hope a free solution).

### Download Choqok 1.3

You can download Choqok 1.3 source code package from [here](http://sourceforge.net/projects/choqok/files/Choqok/choqok-1.3.tar.bz2/download). For Kubuntu users Adilson updated [his PPA](http://bit.ly/ywnxNO) they can update from there. For ArchLinux users, it&#8217;s package is available in [Community repository](http://www.archlinux.org/packages/?repo=Community&q=choqok). (Until now, Survey result told us that %50 of Choqok users are using (K)Ubuntu/ArchLinux)

### Choqok User Survey 2012

The [User survey](http://choqok.gnufolks.org/2012/02/choqok-user-survey-2012/) is still running. Please participate if you didn&#8217;t yet.
  
It&#8217;s result will be published later.

### Support Choqok

You can always support Choqok development via reporting bugs, promoting it, and donating money:

<div align="center">
</div>

#### Known Problem

A bug in Qt 4.8 caused Choqok splash screen sometimes look blank. I hope they fix it and release a bug fix soon.

\___

P.S: The text in Persian at the bottom of splash screen is a wish we make for each other during Nowruz, that means: &#8220;I wish you one hundred years better than these years&#8221; (We wish each other better years ahead.)

P.S 2: Special thanks to [Shahrzad](http://identi.ca/shahrzad), that helped us as always with her great artworks for Choqok.