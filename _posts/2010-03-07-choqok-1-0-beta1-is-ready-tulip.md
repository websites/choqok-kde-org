---
title: 'Choqok 1.0 Beta1 is ready: &#8220;Tulip&#8221;'
date: 2010-03-07T09:20:28+00:00
author: Mehrdad
layout: post
categories:
  - news
---
I&#8217;m proud to bring you first **Choqok** 1.0 beta, a.k.a &#8220;_Tulip_&#8220;.<!--more-->

Long time passed after previous release, I know. The main reasons are some personal problems. BTW, Here it is: <img src="/uploads/choqok0955.png?w=300" alt="" align="right" data-recalc-dims="1" />

## What&#8217;s new in this release:

  * Many of reported and known bugs have been fixed.
  * Actions to subscribe/unsubscribe and block a user, added to User menu. (<span style="font-family: 'Courier';">FEATURE:208126</span>)
  * Choqok startup depends on count of posts shown in any timeline, So with help of [Roozbeh Shafiee (ROSHA)](http://Roozbeh.Us) an Splash screen added to Choqok.
  * There is a new feature in new versions of [StatusNet/Identica](http://status.net) (>0.9) That you can attach a file(such as image) to a dent. Support for this added to Choqok too.
  * Ability to select which timelines we like to have in one account.
  * And 2 new timelines for Twitter and Statusnet/Identica accounts: _Public timeline_ and _Favorites._(Their disabled by default)
  * View a conversation thread, as well as what we had in 0.6.6 version. (<span style="font-family: 'Courier';">FEATURE:194031</span>)
  * Saving search tabs on quit, and re-open them after restart! (<span style="font-family: 'Courier';">FEATURE:221360</span>)
  * Saving read/unread status of posts on quit. [by [Daniel Schaal](http://identi.ca/farbing)]
  * Show avatar of user&#8217;s posts at right instead of left! To distinguish them easier.
  * Optional ReTweet/ReDent prefix. (<span style="font-family: 'Courier';">FEATURE:215855</span>)
  * Configure and About options for shortener plugins!

### New Plugins in this release:

  * &#8220;**ImagePreview**&#8221; plugin to show a thumbnail preview for images. (Currently: Twitpic.com and YFrog.com are supported)
  * &#8220;**Twitpic**&#8221; plugin to upload and share photos to Twitpic.com service!

### New Shortener Plugins:

  * ur1.ca \[by Bhaskar Kandiyal\] (<span style="font-family: 'Courier';">FEATURE:194064</span>)
  * Yourls shortener! \[by Marcello Ceschia\] (<span style="font-family: 'Courier';">FEATURE:221389</span>)

You can download the source package [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.9.55.tar.bz2 "Choqok 1.0 Beta1"), or try binary packages that users and distribution maintainers will deploy.

#### Enjoy Choqok!?

Yes, You can make a donation and help me to keep it up and even better: