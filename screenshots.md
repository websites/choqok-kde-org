---
title: Screenshots
date: 2009-01-19T16:27:23+00:00
author: Mehrdad
layout: page
---
<div id="attachment_10" style="width: 451px" class="wp-caption aligncenter">
  <img class="size-full wp-image-10 " title="choqok" src="/uploads/choqok.png" alt="choqoK Main window" data-recalc-dims="1" />

  <p class="wp-caption-text">
    Choqok Main window
  </p>
</div>

<div id="attachment_15" style="width: 354px" class="wp-caption aligncenter">
  <img class="size-full wp-image-15" title="choqok_quick" src="/uploads/quick-post.png" alt="Choqok Quick Post dialog" data-recalc-dims="1" />

  <p class="wp-caption-text">
    Choqok Quick Post dialog
  </p>
</div>

<div id="attachment_46" style="width: 510px" class="wp-caption aligncenter">
  <a href="/uploads/general-config.png"><img class="size-full wp-image-46" title="choqok_conf3" src="/uploads/general-config-wee.png" alt="Choqok general configuration page" data-recalc-dims="1" /></a>

  <p class="wp-caption-text">
    Choqok general configuration page
  </p>
</div>

<div style="width: 518px" class="wp-caption aligncenter">
  <a href="/uploads/accounts.png"><img title="choqok_conf11" src="/uploads/accounts-wee.png" alt="choqoK Accounts configuration page" data-recalc-dims="1" /></a>

  <p class="wp-caption-text">
    Choqok Accounts configuration page
  </p>
</div>

<div id="attachment_14" style="width: 510px" class="wp-caption aligncenter">
  <br /> <a href="/uploads/plugins.png"><img class="size-full wp-image-48" title="choqok_conf21" src="/uploads/plugins-wee.png" alt="choqoK Plugins configuration page" data-recalc-dims="1" /></a>

  <p class="wp-caption-text">
    Choqok Plugins configuration page
  </p>
</div>
