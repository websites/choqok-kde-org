---
title: Choqok 1.5 released
date: 2015-01-24T07:18:24+00:00
author: Mehrdad
layout: post
categories:
  - news
---
We are happy to announce a new release for Choqok after more than one year.

Big news about this release is that we now have a working plugin for pump.io service. thus Identi.ca users can continue using Choqok. and we kept the StatusNet (Now called [Gnu Social](http://www.gnu.org/software/social/)) plugin, so self hosted Gnu social services are still supported.<!--more-->

### Changes for this release:

  * There&#8217;s a new option to disable the system tray.
  * As mentioned above, we now have a plugin for **Pump.io** microblogs.
  * We have a new plugin for expansion of short URLs, and dropped old **UnTiny plugin**. **
  * A preview for photos posted to **Twitter.com** is now available.
  * Issues in updating friends list of **Twitter** is fixed and sending direct message is working again.
  * There&#8217;s a new option in User menu for Reporting a User to **Twitter**.
  * A long wanted request on **StatusNet(Gnu social) plugin** was ability to take into account the server&#8217;s custom set, char limit. now we have it.
  * **IMStatus plugin** now supports KDE Telepathy.
  * **Yourls** and **Goo.gl** shortening plugins are fixed and now they are working again.
  * Broken shortening plugins are dropped.

I should thank _Andrea Scarpino_, _Daniel Kreuter_ and _Ahmed I. Khalil_ for their contributions to keep the Choqok up.

And [here](https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&f1=cf_versionfixedin&list_id=1194583&o1=equals&product=choqok&query_format=advanced&v1=1.5) you can find a longer list of bugs fixed in this release.

### Download Choqok 1.5

You can download Choqok 1.5 source code package from [here](https://sourceforge.net/projects/choqok/files/Choqok/choqok-1.5.tar.xz/download). For Kubuntu users I think Adilson will update [his PPA](http://bit.ly/ywnxNO).

### Support Choqok

You can always support Choqok development via reporting bugs, promoting it, helping in code and donating money:

<div align="center">
</div>

&nbsp;

_** Regarding UnTiny plugin:_

In this release we replaced the UnTiny plugin with a new plugin(_Expand URLs via longurl.org_) which works better.
  
but if you are upgrading Choqok from a previous installation, it&#8217;s possible that the UnTiny plugin being left on your system, and while they have no conflict with each others, it will be a waste of resource leaving them both to work together. thus I suggest you disabling the UnTiny plugin in &#8220;Plugins&#8221; tab of Choqok Configuration dialog.