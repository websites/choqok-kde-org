---
title: Choqok switched to Git
date: 2010-11-05T02:20:33+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Finally, Choqok switched to Git as its version control solution.

<div>
  Here are the new URLs:</p> 
  
  <div>
    <ul>
      <li>
        Repository browsing: <a href="http://gitweb.kde.org/choqok.git">http://gitweb.kde.org/choqok.git</a> (using gitweb), <a href="http://projects.kde.org/projects/extragear/network/choqok/repository">http://projects.kde.org/projects/extragear/network/choqok/repository</a> (using Redmine)
      </li>
      <li>
        Read only git access: <span style="font-family: monospace;">git clone git://anongit.kde.org/choqok</span>
      </li>
      <li>
        Read/Write git access (for developers) : <span class="Apple-style-span" style="font-family: monospace;">git clone git@git.kde.org:choqok</span>
      </li>
    </ul>
  </div>
</div>