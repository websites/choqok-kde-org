---
title: Choqok User Survey 2012
date: 2012-02-27T03:34:19+00:00
author: Mehrdad
layout: post
categories:
  - news
---
[<img class="alignright size-full wp-image-330" title="Choqok User Survey 2012" src="/uploads/splash_screen2.png?fit=300%2C195" alt="" srcset="/uploads/splash_screen2.png?w=300 300w, /uploads/splash_screen2.png?resize=150%2C97 150w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />](https://docs.google.com/spreadsheet/viewform?formkey=dERoNlZWNFFnUHFTRGNxSS1nM3NqWnc6MQ#gid=0)Recently, We wanted to do some decisions for Choqok road map and what to do next? We found out that after more than 3 years, Choqok now has a growing number of users, But unfortunately we don&#8217;t have a good view of them? what distro, what desktop do they use? and what do they use and need more?<!--more-->

For example until now, Our expectation of Choqok users was that they all are KDE users. but recently I realized that we have Gnome users too. Or Until now, all of our efforts was for Gnu/Linux users, however KDE is available on Windows too, So how about Windows? Do we need to port Choqok there too?

So, We ended up that we need a survey to have better image from Choqok users community. And here we are, We prepared some questions that seems useful for those decisions, and [here you can find and participate in the survey](https://docs.google.com/spreadsheet/viewform?formkey=dERoNlZWNFFnUHFTRGNxSS1nM3NqWnc6MQ#gid=0).

Please spread the word and tell your friends (I mean, Choqok users :D) to fill it up. I think it cannot take more than 10 minutes at all. So please help us to know your needs better, and have a better client.

&nbsp;

Thanks for your cooperation.
  
Your Choqok team.