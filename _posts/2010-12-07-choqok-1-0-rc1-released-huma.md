---
title: 'Choqok 1.0 RC1 released: &#8220;Huma&#8221;'
date: 2010-12-07T03:26:12+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 1.0 RC1 codenamed &#8220;[<span class="Apple-style-span">Huma</span>](http://en.wikipedia.org/wiki/Huma_bird)&#8221; released, And it&#8217;s the last step towards version 1.0

We planned to release the final version 1.0 at 26-Dec (Its Second Birthday Anniversary), So with this release Choqok entered into a hard feature freeze. And we&#8217;ll be happy to get bug reports. So don&#8217;t hesitate on bug reporting.

<!--more-->

You can download and build the source code for this release from [here](http://choqok.gnufolks.org/pkgs/choqok_0.9.98.tar.bz2). [<img title="OCS Plugin" src="/uploads/choqok-ocs.png?resize=200%2C283" alt="OCS Plugin" align="right" data-recalc-dims="1" />](/uploads/choqok-ocs.png)Or check [download](http://choqok.gnufolks.org/download/) page for binary packages.

### We have 5 new plugins with this release:

<div>
  <div>
    <ol>
      <li>
        OCS Plugin to support &#8220;Open Collaboration Services Activities API&#8221; (For opendesktop.org)
      </li>
      <li>
        IMStatus plugin to set your latest post to status message in IM clients (Kopete, Pidgin, Psi and Skype)
      </li>
      <li>
        Twitgoo image uploader
      </li>
      <li>
        Mobypicture image uploader
      </li>
      <li>
        Posterous image uploader
      </li>
    </ol>
    
    <h3>
      Other changes since previous beta release are:
    </h3>
    
    <ul>
      <li>
        Support for Twitter Lists
      </li>
      <li>
        Support for LibIndicate to use as an alternate notification system. (It&#8217;s Optional)
      </li>
      <li>
        Option to show timelines in reverse order
      </li>
      <li>
        Additional action in input boxes&#8217; context menu that will shorten all selected URLs or all URLs if there&#8217;s no selection.
      </li>
      <li>
        &#8220;Hide Post&#8221; action for post widgets. So, You can hide one post or all posts from a person! Via Context menu.
      </li>
      <li>
        BiDi support is back.
      </li>
      <li>
        Support for Twitgoo service in imagepreview plugin. (Alex Infantes)
      </li>
      <li>
        Digg shortener plugin removed: &#8220;As of May 17th, 2010, the Digg short URL feature will be available only for URLs that refer to stories submitted to Digg&#8221; <a href="http://about.digg.com/blog/update-diggs-short-url-service">http://about.digg.com/blog/update-diggs-short-url-service</a>
      </li>
      <li>
        Send reply to original author of repeated posts + cc to repeater.
      </li>
      <li>
        And there&#8217;s other fixes and improvements around current functionalities.
      </li>
    </ul>
    
    <div>
      And with this release we have two new <strong>optional</strong> dependencies: <a href="https://projects.kde.org/projects/kdesupport/attica">libAttica</a> for OCS plugin and <a href="https://launchpad.net/libindicate-qt">libIndicate-qt</a> for Indicator notifies support.
    </div>
  </div>
</div>