---
title: 'Choqok 1.0 Alpha2 released: &#8220;Razi&#8221;'
date: 2009-10-15T11:10:55+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 1.0 alpha2, codenamed &#8220;[Razi](http://en.wikipedia.org/wiki/Muhammad_ibn_Zakariya_al-Razi "Muhammad ibn Zakariya al-Razi")&#8220;, released with many improvements and fixes around previous alpha1. And it&#8217;s another big step toward Choqok 1.0.

<!--more-->

### What&#8217;s new?

[<img title="Choqok" src="/uploads/choqok1.png?w=300" border="0" alt="Choqok" align="right" data-recalc-dims="1" />](/uploads/choqok1.png)

  * Major change since alpha1 is search api implementation, that embedded in main window for better usage!
  * &#8220;Who is&#8221; dialog, that you can see it by clicking on usernames and select &#8220;Who is USER&#8221; is back with more information and useful actions:
  * You can **Subscribe** (Follow), **Unsubscribe**(Unfollow) to a user timeline or **Block** a user from within &#8220;Who is&#8221; dialog.
  * UnTiny function implemented as a plugin, that you can disable it if don&#8217;t like it. and It will schedule its works to make cpu usage less on startup!
  * Shortener plugin for Is.gd added. (Felix Rohrbach)
  * &#8220;Open in web browser&#8221; symbol changed to ☛.
  * Option for changing &#8220;exclamation mark (!)&#8221; on group names, when re-sending a post added to StatusNet account config dialog, to prevent spamming on groups!

You can download source package from [here](http://www.ohloh.net/p/choqok/download?filename=choqok-0.9.4.tar.bz2)!

&#8212;&#8212;

P.S: [Razi](http://en.wikipedia.org/wiki/Muhammad_ibn_Zakariya_al-Razi) was a Persian alchemist, chemist, physician, philosopher and scholar. And today is his death anniversary!