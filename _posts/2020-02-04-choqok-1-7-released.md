---
title: Choqok 1.7.0
date: 2020-02-04T22:00:00+00:00
author: ilpianista
layout: post
categories:
  - news
---
Long time no see, Choqok users!

First of all Choqok has a new and shiny website. Kudos to Carl Schwan for taking care of the theme!

To me, version 1.7.0 was meant to be released more than one year ago, while I just released it today.

The main reason of the delay (a part from lack of time) is because I wanted 1.7.0 to be bullet proof (spoiler: it's not).

I wanted Choqok 1.7.0 to have full Mastodon support, proper media attachments and a lot more.

Let's try to start somewhere. With this version I want to close a Choqok era and prepare us for the next one. Stay tuned!

### Changes for this release:

  * Port to QtNetworkAuth and drop qoauth dependency
  * Allow to disable accounts
  * Honour the default font [#372291](https://bugs.kde.org/show_bug.cgi?id=372291)
  * Make the sign footer consisent between all microplugins
  * Unread post count in the application title sums all unread posts' accounts
  * Twitter: update char limit to 280
  * Twitter: support extended tweets [#370260](https://bugs.kde.org/show_bug.cgi?id=370260)
  * Twitter: fix list browsing [#382392](https://bugs.kde.org/show_bug.cgi?id=382392)
  * Twitter: fix followers list
  * Twitter: show client source even for private messages
  * Twitter: show user' real name when no description is set
  * GNU Social: do not rely over qvitter to get the post url
  * GNU Social: hide linkback statuses
  * GNU Social: show user' real name when no description is set
  * Pump.io: escape the description
  * Pump.io: show user' real name when no description is set
  * Drop yFrog support from ImagePreview plugin
  * Plugin compability break: MicroBlog::profileUrl returns a QUrl instead
  * Plugin compability break: MicroBlog::postUrl returns a QUrl instead

Thank to (random order): _Andrea Scarpino_, _Nicolas Fella_, _Luca Beltrame_, _Luigi Toscano_, _Pino Toscano_, _Heiko Becker_, _Andreas Sturmlechner_, _Yuri Chornoivan_ for their contributions to keep Choqok up.

And [here](https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&f1=cf_versionfixedin&list_id=1194583&o1=equals&product=choqok&query_format=advanced&v1=1.7) you can find a longer list of bugs fixed in this release.

### Download Choqok 1.7

You can download Choqok 1.7 source code package from [here](http://download.kde.org/stable/choqok/1.7/src/choqok-1.7.0.tar.xz). For Kubuntu users I think Adilson will update [his PPA](https://launchpad.net/~adilson/+archive/ubuntu/experimental).

### Support Choqok

You can always support Choqok development via reporting bugs, translating it, promoting it, helping in code and donating money.
