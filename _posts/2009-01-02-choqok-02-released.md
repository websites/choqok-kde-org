---
title: Choqok 0.2 released
date: 2009-01-02T17:11:30+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 0.2 with following changes and some improvements and bug fixes released.

<!--more-->

the change log is:

  * Fetching User Images moved to background and make it asynchronous.
  * Support for batch notifying added.
  * Support for using secure connection (HTTPS) added.
  * Support for KNotification implemented.
  * Source code issues reported by Krazy fixed.
  * Bug fix on saving statuses list.
  * Support for shortening urls with more than 30 characters implemented.
  * The back-end functions migrated to KIO library.

Download it [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.2.tar.gz)