---
title: 'Choqok 1.0-Beta2 released: &#8220;Reloaded&#8221;'
date: 2010-08-15T08:30:34+00:00
author: Mehrdad
layout: post
categories:
  - news
---
One year passed after latest stable release. But long time with so many changes and features for our Micro-Blogging client.

However this is not the officially stable 1.0 release, But <span style="text-decoration: underline;">it&#8217;s quit stable and usable now</span>! And because of changes at twitter and Identica land, We highly recommend packing and using this version instead of 0.6.6. (One of the big changes in Twitter is Migrating authentication to OAuth method)

First I have to thank 2 groups of people who helped me on this release:
  
1. People who [asked for new features they want](http://choqok.gnufolks.org/forum/ "Speedup Choqok Development Forum"), and contributed money to have them sooner.
  
2. People who helped me by fixing bugs and implementing their ideas in code.

So, Here we go:
  
**Choqok 1.0 Beta2 (0.9.85)** codenamed &#8220;**_Reloaded_**&#8221; is ready to use and distribute 🙂

<!--more-->

## New features on this release:

Changes instead of previous version [1.0Beta1] :

<p style="-qt-block-indent: 1;">
  <ul>
    <li>
      New plugin system to make support for more image/file hosting services easier. (Uploader plugin system) :
    </li>
  </ul>
  
  <p>
    <img class="aligncenter" src="/uploads/uploader2.png" alt="" align="center" data-recalc-dims="1" />
  </p>
  
  <p style="-qt-block-indent: 1;">
    <ul>
      <li>
        Complete support for ReTweet/Repeat function. <a href="http://choqok.gnufolks.org/forum/topic/9">+</a>
      </li>
      <li>
        Nick name (username) auto-completion implemented per account base. (Available in main window input boxes) <a href="http://choqok.gnufolks.org/forum/topic/4">+</a>
      </li>
      <li>
        D-Bus Interface (By Emanuele Bigiarini)
      </li>
      <li>
        A konqueror plugin to use Choqok directly (post selected text, share link & upload media service menus (By Emanuele Bigiarini)
      </li>
      <li>
        OAuth support for Twitter and Identica authentication. (Using <a title="QOAuth" href="http://qt-apps.org/content/show.php/QOAuth?content=107420">QOauth</a> Lib (>=1.0.1), It&#8217;s a dependency for Choqok now)
      </li>
      <li>
        Proxy configuration. (Using KDE global proxy)
      </li>
      <li>
        Using the new KDE tray icon.
      </li>
      <li>
        Support for Amarok, JuK, Exaile, Rhythmbox, Audacious, Dragon Player, Qmmp, VLC, Banshee and any other MPRIS compatible player in &#8220;Now Listening&#8221; plugin. (By Ramin Gomari & Andrey Esin)
      </li>
      <li>
        File attachment function(button) added to QuickPost (Using global uploader dialog).
      </li>
      <li>
        Additional actions for User menu: Reply, Send Private Message, Subscribe/UnSubscribe, Block
      </li>
      <li>
        Support for Tweetphoto in ImagePreview plugin (By Alexandro Infantes)
      </li>
      <li>
        Better URL detection (By Andrey Esin)
      </li>
      <li>
        Ctrl+P shortcut (Quick Post) removed and it&#8217;s function merged with Ctrl+Z
      </li>
      <li>
        Revised posts look
      </li>
      <li>
        Many known and found bugs fixed.
      </li>
      <li>
        <a href="http://qjson.sourceforge.net/">QJson</a> Lib is a new dependency for Choqok too! Some plugins using it.
      </li>
    </ul>
    
    <p>
      <strong>New plugins:</strong>
    </p>
    
    <p style="-qt-block-indent: 1;">
      <ul>
        <li>
          Post Filtering (To filter out unwanted posts) Configurable based post text, author username, reply to user and source client. Regular Expressions supported too. <a href="http://choqok.gnufolks.org/forum/topic/12">+</a>
        </li>
        <li>
          Preview Videos (Supports YouTube & Vimeo) (By Emanuele Bigiarini)
        </li>
      </ul>
      
      <p>
        <strong>New Shortener Plugins:</strong>
      </p>
      
      <p style="-qt-block-indent: 1;">
        <ul>
          <li>
            ur.ly (A free [Apache 2.0 licensed] URL shorting service) (By Scott Banwart)
          </li>
          <li>
            u.nu (By Timothy Redaelli)
          </li>
          <li>
            urls.io (By Boris Tsirkin)
          </li>
          <li>
            goo.gl (By Andrey Esin)
          </li>
          <li>
            3.ly (By Andrey Esin)
          </li>
        </ul>
        
        <p>
          <strong>Uploader Plugins:</strong>
        </p>
        
        <p style="-qt-block-indent: 1;">
          <ul>
            <li>
              TwitPic.com
            </li>
            <li>
              YFrog.com
            </li>
          </ul>
          
          <p>
            And, <em>Of course</em>! there are too many features and changes since version 0.6.6 🙂
          </p>
          
          <h2>
            Download
          </h2>
          
          <p>
            You can download the source code from <a title="Choqok-0.9.85" href="https://www.ohloh.net/p/choqok/download?filename=choqok-0.9.85.tar.bz2&projects_id=choqok">here</a>, and follow the build/install construction (in README file) to build and install Choqok.<br /> Or, You can wait for distribution packagers to package it for you.
          </p>
          
          <p>
            <strong>Important note for Twitter users:</strong>
          </p>
          
          <p>
            Twitter guys <a title="Twitter API & the Basic Auth Shutdown: Everything you need to know." href="http://groups.google.com/group/twitter-development-talk/t/c03fa2b1ae90d2a9">announced</a> that they will shutdown the basic authentication from 16 to 30 Aug, at a process of decreasing rate limit. So you have to migrate and use the new Choqok until then! Older ones won&#8217;t work with Twitter after 30 Aug.
          </p>
          
          <h4>
            Enjoy Choqok!?
          </h4>
          
          <p>
            You can make a donation and support us to keep it up and even better:
          </p>