---
title: 'Choqok 1.0 Beta4 released: &#8220;Wind&#8221;'
date: 2010-10-15T13:43:55+00:00
author: Mehrdad
layout: post
categories:
  - news
---
<p align="justify">
  Choqok 1.0 Beta4 (codenamed "Wind") proudly released With so many fixes around previous betas and some new features and plugins.
</p>

<p align="justify">
  You can get the source code of this release <a href="http://www.ohloh.net/p/choqok/download?filename=choqok-0.9.92.tar.bz2&#038;projects_id=choqok" title="Choqok 1.0 Beta4">here</a>, or wait for binary packages that your distro maintainers will provide you.
</p>

<!--more-->

<p align="center">
  <a href="/uploads/choqok-whois3.png"><img src="/uploads/choqok-whois3.png" data-recalc-dims="1" /></a>
</p>

### Change summary:

  * Prevent shortening process to freeze UI
  * Remove the ☛ sign for tags or groups in StatusNet accounts, using the menu solution like @username
  * "Configure Notifications" in Settings menu.
  * Twitpic plugin fixed to work with OAuth.
  * "Reply to USER", "Write to USER", "Reply to all" actions added to reply button menu.
  * New look for Whois dialog.
  * Dictionary switcher in text input fields via context menu.
  * StatusNet: Info for users across multiple status.net servers
  * Filter plugin: Option to just show own replies or just show replies to my friends
  * Image Preview plugin: Support for img.ly and Plixi (By Alex Infantes)
  * + Many of known and reported bugs fixed.

New Plugins:

  * [ImageShack](http://imageshack.us) uploader (Just image uploading)
  * [Flickr](http://www.flickr.com/) uploader (Just image uploading)

We&#8217;ve removed some plugins:

  * u.nu shortner, due its service discontinued
  * YFrog uploader (temporary)