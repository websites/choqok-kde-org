---
title: Choqok 0.6.1 released
date: 2009-06-13T11:47:04+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 0.6.1 codenamed &#8220;**The Dark Saturday**&#8221; released.

<!--more-->

Due to [new situation](http://www.twitpocalypse.com/) on [twitter.com](http://twitter.com):

> The unique identifier associated to each tweet has now exceeded 2,147,483,647. For some of your favorite third-party Twitter services not designed to handle such a case, the sequence will suddenly turn into negative numbers. At this point, they are very likely to malfunction or crash.

We decided to have a bug fix release to fix such a problem.
  
This upgrade is highly recommended for Twitter users.

Download source code [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.6.1.tar.bz2).

Have fun,
  
Mehrdad