---
title: Choqok 0.3.1 released
date: 2009-01-13T17:22:01+00:00
author: Mehrdad
layout: post
categories:
  - news
---
The 0.3 and 0.3.1 released together, because of a bug on 0.3 we release 0.3.1 after some hours.

<!--more-->

Changes on 0.3.x series:

  * Support for Multiple Accounts implemented.
  * Support for Identi.ca service implemented. (Using its Twitter compatible API)
  * Some information about user and Links added to User Interface via tooltips.
  * The shape of post new status field on mainWindow changed to be more user friendly and more usable.
  * Ability to Post new status to All available accounts implemented on Quick tweet tool.
  * KWallet integration implemented.
  * Media files manager improved.
  * Support for Enabling or Disabling Spell checker by a shortcut key on new Tweet fields added. (Ctrl+S)
  * Some reported and known bugs fixed.