---
title: 'Choqok 0.6.6 Released: &#8220;Let&#8217;s Be Green&#8221;'
date: 2009-08-06T06:41:24+00:00
author: Mehrdad
layout: post
categories:
  - news
---
During my works to make Choqok more extendable, some fixes and improvements by Choqok great contributers, &#8220;[Tejas Dinkar](http://twitter.com/tdinkar)&#8221; and &#8220;[Daniel Schaal](http://identi.ca/farbing)&#8221; helped me to make Choqok 0.6.6 a.k.a &#8220;**Let&#8217;s Be Green**&#8221; available to you.

<!--more-->

Major changes and fixes are:<img src="/uploads/choqok-greenish.png" alt="" align="right" data-recalc-dims="1" />

  * Icon design improved and made greenish. (Roozbeh Shafiee)
  * Ability to follow a thread (Or option to see all previous posts of a conversation) (Tejas Dinkar)
  * Ability to resolve a TinyURL into a regular URL on url tooltip. (Tejas Dinkar)
  * Replaced &#8220;RT&#8221; for ReTweet function by ♻-Symbol. (as requested by users)
  * Identica hashtag search problem fixed and works again. (And it&#8217;s a full text search like Twitter custom)
  * Ability to cycle through accounts using the scroll wheel on the system tray icon (Daniel Schaal)

You can get source package from [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.6.6.tar.bz2 "Choqok-0.6.6.tar.bz2"), or wait for binary packages.

Enjoy.

&#8212;-

_A Note for svn users_: After this release trunk will go to an alpha stage for version 1.0 because of many changes. And some features will disable for now!