---
title: Choqok 1.6 Beta 2 released
date: 2016-08-04T09:13:26+00:00
author: ilpianista
layout: post
categories:
  - news
---
We are happy to announce a new upcoming release for Choqok after more than one year and half.

We want to make this release more stable than ever and then we are going to release a beta today and the final version next month.

Big news about this release is that Choqok is now based on KDE Frameworks 5 and we officially support Friendica.

### Changes for this release:

  * Rename StatusNet microblog as GNU Social
  * Twitter: fix user lists loading
  * Twitter: allow to select any follower when sending a direct message
  * Twitter: fix searches by username
  * Twitter: fix searches by hashtag
  * Twitter: show original retweet time
  * Twitter: fix external URL to access direct messages and tweets
  * Twitter: send direct message without text limits
  * Twitter: support to send and view tweets with quoted text
  * Twitter: allow to delete direct messages
  * GNU Social: fix medium attachment to post
  * GNU Social: allow to send direct messages
  * Fix removal of accounts with spaces in their names
  * Fixed the bug that overwrite an account if you use the same alias of another
  * Always use HTTPS when available
  * ImageView: dropped Twitpic and Tweetphoto support (services are dead)
  * A couple of segmentation fault fixes

I should thank _Mehrdad Momeny,_ _Andrea Scarpino_, _Ignacio R. Morelle_, _Ian Schwarz_, _Gilbert Assaf_ for their contributions to keep the Choqok up.

And [here](https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&f1=cf_versionfixedin&list_id=1194583&o1=equals&product=choqok&query_format=advanced&v1=1.6) you can find a longer list of bugs fixed in this release.

### Download Choqok 1.6 Beta 2

You can download Choqok 1.6 Beta 2 source code package from [here](http://download.kde.org/unstable/choqok/1.5.85/src/choqok-1.5.85.tar.xz). For Kubuntu users I think Adilson will update [his PPA](https://launchpad.net/~adilson/+archive/ubuntu/experimental).
  
Note that qoauth-qt5 is required! Choqok cannot run with qoauth-qt4 anymore.

### Support Choqok

You can always support Choqok development via reporting bugs, translating it, promoting it, helping in code and donating money.