---
title: 'Vote Choqok as &#8220;Best new project&#8221;'
date: 2009-06-23T11:31:20+00:00
author: Mehrdad
layout: post
categories:
  - news
---
As you may know, Voting for [Sourceforge.net](http://Sourceforge.net) [Community Choice Award 2009](http://sourceforge.net/community/cca09/) is open from Yesterday (22-Jun) and will be open until 20 July.

<!--more-->

<p align="center">
  <a href="http://sf.net/community/cca09/vote/?f=451"><img src="http://sf.net/awards/cca/badge_img.php?f=451" /></a>
</p>

This year, [**Choqok**](http://choqok.gnufolks.org) is one of nominates to be the &#8220;_Best New Project_&#8221; winner.

So, If you enjoy **Choqok**, And think it deserves that, go [here](http://sf.net/community/cca09/vote/?f=451) and Vote **Choqok** as &#8220;_Best New Project_&#8220;.

Have fun,  
Mehrdad