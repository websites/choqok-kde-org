---
title: choqok 0.4 released
date: 2009-02-11T20:54:14+00:00
author: Mehrdad
excerpt: After a month of development, choqoK 0.4 with bunch of improvements is now ready to use.
layout: post
categories:
  - news
---
After a month of development, Choqok 0.4 with bunch of improvements is now ready to use.<!--more-->

**Major changes:**

  * Support for all functionalities of &#8220;Direct Messages&#8221; implemented.
  * support for recognizing link of Identi.ca&#8217;s Tags and Groups implemented.
  * First run wizard.
  * Ability to Enable/Disable Notifications and Updating time-lines via system tray context menu.
  * Support for TightURL (a free url shortening service) implemented.
  * Bunch of known bugs fixed.

Also some of users requested features implemented, some of them are at minor changes section:

**Minor changes:**

  * The &#8220;Quick Tweet&#8221; widget will remember its size.
  * A global shortcut (Meta+Ctrl+C) sets for Toggle mainwindow visibility.
  * Support for using secure connections (HTTPS) is Back, it was disabled because the Identi.ca server doesn&#8217;t support it before.
  * Result of posting new status with Quick Tweet, will effect on system tray Icon.
  * Add &#8220;Mark All As Read&#8221; action to the File Menu. (Thanks to &#8220;Casey Link&#8221; for patch)
  * An &#8220;in reply to&#8221; link added to sign of reply statuses, and @USER links points to user account (Instead of main notice/status)

[Download source code from here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.4.tar.bz2)

Thanks to all users  that report bugs, and send their feature requests to me.