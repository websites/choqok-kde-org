---
title: Speeding Up Choqok Development
date: 2010-04-11T19:34:50+00:00
author: Mehrdad
layout: post
categories:
  - news
---
We are proud to be with you for more than one year with Choqok.

As you know, Choqok is a free/open source application!<!--more-->

It&#8217;s development was fast before. But As you noticed, The development speed has been reduced for several months.

But we have a new idea to speed it up again, and have a great 1.0 release as soon as possible.

I have written 2 posts about this idea, and about how it works, and how users can participate in it. [One that explains the idea](http://momeny.wordpress.com/2010/03/28/an-idea-to-speed-up-choqok-development/) and [a newer that announces start of it](http://momeny.wordpress.com/2010/04/09/lets-extend-choqok-together/).

**In short:**

  1. Someone starts a new topic in [_New Idea_](http://choqok.gnufolks.org/forum/forum/1) forum with information about what&#8217;s in his/her mind to be implemented in Choqok.
  2. He/She announces it anywhere thinks that interested people can find it (i.e. [Identi.ca](http://identi.ca/)) to get here and help us in development.
  3. Then we (interested users and me) will discuss about it and it&#8217;s implementation and will set the donation amount it requires to be implemented.
  4. Now we have to collect donation for it: Anyone who wants to have that feature **sooner** (and likes to help to its development) can donate to it. He/She should note in donation comment about what feature/idea he/she donates to!
  5. When donation is about to reach the requirement, I will start to implement the idea, and it&#8217;s topic in forum will be moved to [_Under Development_](http://choqok.gnufolks.org/forum/forum/3) forum. still we can discuss about it there!
  6. And when its implementation is done! I&#8217;ll announce it there(in topic) and move topic to [_Implemented_](http://choqok.gnufolks.org/forum/forum/2) forum.

_I hope to have a faster progress for Choqok, with your help._