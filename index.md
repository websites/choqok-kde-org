---
date: 2010-07-11T16:46:59+00:00
author: Mehrdad
layout: page
---

Choqok (pronounced: tʃœˈʁʊk) is a [Free](https://en.wikipedia.org/wiki/Free_software)/[Open Source](https://en.wikipedia.org/wiki/Open_source_software) micro-blogging client by the [KDE community](https://kde.org).

The name comes from an ancient Persian word, means Sparrow!

Currently supports [Twitter.com](https://twitter.com/), [GNU Social (formely known as StatusNet)](https://www.gnu.org/software/social/), [Pump.io (formerly known as Identi.ca)](https://pump.io/), [Friendica](https://friendi.ca/) and [Open Collaboration Services](http://www.open-collaboration-services.org/) (used by [OpenDesktop.org](https://www.opendesktop.org)).

[<img src="/uploads/choqok-wee.png" alt="" align="right" border="0" data-recalc-dims="1" />](/uploads/choqok.png)

### It currently features:

  * Supporting Twitter.com micro-blogging service.
  * Supporting self hosted Pump.IO micro-blogging service.
  * Supporting self hosted GNU Social websites. (by using their Twitter compatible API)
  * Supporting self hosted Friendica websites. (by using their GNU Social compatible API)
  * Supporting Open Collaboration Services API.
  * Supporting Friends, @Reply, Favorite and Public time-lines.
  * Support for send and receive direct messages.
  * Official Repeat/ReTweet functions.
  * Supporting Multiple Accounts simultaneously.
  * Supporting search APIs for Twitter and StatusNet services.
  * Support for Twitter lists.
  * KWallet integration.
  * Ability to make a quick tweet/dent with global shortcuts. (Ctrl+Meta+T)
  * Ability to notify user about new statuses arriving.
  * Flickr, ImageShack, Twitpic, Twitgoo, Mobypicture and Posterous photo uploading
  * Support for shortening urls with more than 30 characters.+shorten on paste, With more than 10 shortening services
  * Posts (Statuses) list appearance configuration.
  * Filtering supported to hide unwanted posts.
  * Setting your last post as your current status message in IM clients (Kopete, Pidgin, Psi, Skype)
  * Preview Images from Twitter, Twitpic, YFrog, img.ly, plixi, Twitgoo, TweetPhoto and etc. services.
  * Preview Videos from YouTube and Vimeo services.
  * Send Now Listening text. (Many of favorite players such as Amarok, Exaile, Banshee, Rhythmbox and VLC supported)
  * Expand short Urls.
  * Proxy support.

### License:

This project is licensed under the [GNU GPL v3](https://www.gnu.org/licenses/gpl.html) license.
