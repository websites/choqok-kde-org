---
title: 'Choqok 1.0 Alpha1 released: &#8220;Reloading&#8221;'
date: 2009-09-13T14:23:35+00:00
author: Mehrdad
layout: post
categories:
  - news
---
First preview of what Choqok 1.0 will be a.k.a Choqok 1.0 Alpha1 is ready. (Codenamed &#8220;Reloading&#8221;)

<!--more-->

One of important TODOs for Choqok was making it plugin aware, And now, It&#8217;s new, plugin base design is ready for developers who like to join in its development.<img src="/uploads/choqok-1-alpha1.png" alt="" align="right" data-recalc-dims="1" />

Currently there are 3 types of plugins we can extend Choqok with. You can find some useful information about how to write a plugin in [gitorious wiki pages](http://gitorious.org/choqok/pages/Home), and 2 new pages added to [Choqok website](http://choqok.gnufolks.org), [Development](http://choqok.gnufolks.org/development/), and [TODO List](http://choqok.gnufolks.org/development/todo/). They are all useful for developers to join in extending Choqok.

### What&#8217;s new for users?

Most of changes on this release are sensible by developers, but there are some for users:

Some features did not implemented in new design, **that you may miss on upgrade**:

  * Search API implementation. (So clicking on user names, group names and hashtags will do nothing!)
  * Notifies for new posts.
  * Twitpic.com integration.
  * &#8220;Who is&#8221; and unTiny

New features on new design are:

  * Ability to mark one post, timeline or blog as read.
  * Ability to change Accounts order on main window.
  * Ability to add an account read only, and don&#8217;t show an account on &#8220;quick post&#8221; dialog.
  * Identica and Laconica entries on Accounts management, merged and now you can see them under &#8220;StatusNet&#8221; name, the new name of Laconica.
  * And some other less sensible changes&#8230;

And you have to re add your accounts because of changes&#8230;

### Should I upgrade?

however it&#8217;s a developer preview, But it&#8217;s quit stable.

If you want to try new changes and don&#8217;t need leaked features, an upgrade may be funny, otherwise, I recommend you to stay with previous stable release (0.6.6) and wait for a later version&#8230;

### Download

You can get the source code from [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.9.1.tar.bz2).

### Icon color change

I was launch [a poll](http://momeny.wordpress.com/2009/09/03/choqok-icon-design/) about Icon color between Green and Blue, and you choose the green one to be official icon. Thanks to all you for participating on it.

**Poll** [**results**](http://answers.polldaddy.com/poll/1943803/)**:**

Total votes: 609
  
Green Icon: 318
  
Blue Icon: 291