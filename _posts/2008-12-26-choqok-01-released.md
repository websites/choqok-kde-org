---
title: Choqok 0.1 released
date: 2008-12-26T17:01:36+00:00
author: Mehrdad
layout: post
categories:
  - news
---
First public release of Choqok with the following features is available

<!--more-->

  * Support for User + His/Her Friends time-lines
  * Support for @Reply time-lines
  * Ability to make a quick tweet with global shortcuts (Ctrl+Meta+T) and like other KDE apps, its shortcuts are configurable.
  * Ability to notify user about new statuses text, with Libnotify&#8217;s notify-send command (without any unwanted dependency to it) (It will support for KDE Notifications on next releases)
  * Support for showing User pics.
  * Support for configuring status lists appearance.
  * Currently just support for one user account. (it will support multiple users on next releases)

Download from [here](https://www.ohloh.net/p/choqok/download?filename=choqok_0.1_src.tar.gz)