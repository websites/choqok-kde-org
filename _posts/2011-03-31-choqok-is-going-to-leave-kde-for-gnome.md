---
title: Choqok is going to leave KDE for Gnome
date: 2011-03-31T19:58:18+00:00
author: Mehrdad
layout: post
categories:
  - news
---
<!-- 		@page { margin: 0.79in } 		P { margin-bottom: 0.08in } -->

<span style="font-size: medium;">Is this really Cute? I don&#8217;t think so.</span>

<span style="font-size: medium;">We all have recently heard of what Nokia did to ambitions of thousands of developers especially Qt developers around the world by practically discontinuing support on MeeGo and Qt. Although they have many times acknowledged that we are not going to end the MeeGo/Qt era but what we are so certain about business people is that they are not going to tell true stories to children. So to be honest I do not feel good about the future of Qt in the hands of devil.<!--more--></span>

<span style="font-size: medium;">But the story does not end here, on the other hand we are developing Choqok in the KDE planet and by planet I mean a large amount of people who are doing free software stuff and producing very beautiful buggy systems. Although it has been improved very good over time but as developers we feel it is time to get some rest and take a shower, bugs have made us look ugly in the eyes of community. So we have to confess that we are tired on desktop environment bugs being counted as our bugs. We are going to find the stable alternative.</span>

&nbsp;

<span style="font-size: medium;">You know what? even if we hoped that everything will be fixed in near future we still would somehow persuade ourselves to stay in this inappropriate situation, but as we look back into history we find that KDE people don&#8217;t look stable, they are going to repeat what happened in Japan on their own systems every minute. if you are thinking why the hell would they do so just think back a minute to those stable days of KDE 3 and what happened to users since the beginning of KDE 4 era.</span>

&nbsp;

<span style="font-size: medium;">And when you have all what I said on one hand you just need Gnome on the other hand to do the decision making easy. we were worried about the future and what are the big enterprises like Nokia are going to do to our platform and software, yes? by moving to GTK we are all by ourselves. We were too afraid of whether users after experiencing this much trouble and lack of good support will still rely on our system, weren&#8217;t we? I do not think anybody in the world ever think of unreliability after hearing the word Gnome.</span>

<span style="font-size: medium;">So to make a long story short,we are officially announcing the move from KDE/Qt to Gnome/GTK for our child <strong>Choqok</strong>. This decision will definitely bring some months without good support and with problems but future is so promising. By the way we are adopting gnome manner in our software from now on, So there would be no more features for at least one year and we will work on stability instead.</span>

&nbsp;

<span style="font-size: medium;">P.S: In Iran it is New Year holidays now, and in our culture we usually renew everything we can on the new year event. maybe it made the move easier.</span>

<span style="font-size: medium;">&#8212;</span>

<span style="font-size: medium;"><strong>Update</strong>: This was just an April fools joke 😉<br /> </span>