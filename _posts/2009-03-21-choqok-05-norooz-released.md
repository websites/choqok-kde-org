---
title: Choqok 0.5 (Norooz) released
date: 2009-03-21T22:31:09+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 0.5, codenamed &#8220;[Norooz](http://en.wikipedia.org/wiki/Norooz "Norooz at Wikipedia")&#8221; is ready to use.

<!--more-->

There are many improvements and new contributers for this version, many thanks to contributers.

**Major changes:** (with contributer name)

  * Twitter.com search API implemented. (Stephen Henderson)
  * Identi.ca search API implemented. (Stephen Henderson)
  * Integration of search facilities in MainWindow. and less need of using web browser. (Daniel Schaal)
  * Support for Laconica websites implemented.
  * Showing base status of a reply status on Choqok window.
  * Icon updated.
  * Many improvements on UI. (Daniel Schaal)
  * Support for shortening urls on paste. (Stephen Henderson)

Download the source code from [here](https://www.ohloh.net/p/choqok/download?filename=choqok-0.5-src.tar.bz2 "choqoK at OSPDev"). Binary packages for distributions will be available at repositories.