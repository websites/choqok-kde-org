---
title: Download
date: 2009-01-19T15:51:49+00:00
author: Mehrdad
layout: page
---
## Latest Release: Choqok 1.6

  * **Source code:** [choqok-1.6.tar.bz2](https://download.kde.org/stable/choqok/1.6/src/choqok-1.6.0.tar.xz)
  * **Binary Packages**(Community maintained) :
      * [ArchLinux](https://www.archlinux.org/packages/community/x86_64/choqok/)
      * [Kubuntu PPA](https://launchpad.net/~adilson/+archive/ubuntu/experimental)
      * [Gentoo](https://packages.gentoo.org/packages/net-im/choqok)
      * Slackware: [SlackBuild](https://github.com/denydias/slackbuilds/tree/master/testing/choqok)

<p class="text-center">
  ***
</p>

## Latest development snapshot:

To get latest development snapshot use this:

```bash
$ git clone git://anongit.kde.org/choqok
```

And if you already cloned the repository:


```bash
$ git pull
```

Please take a look at README file, for build instruction.

### Requirements to build:

* CMake 2.8.12
* Qt 5.9
* KDE Frameworks libraries 5.6
* QCA2-Qt5 library
