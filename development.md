---
title: Development
date: 2009-03-27T09:41:40+00:00
author: Mehrdad
layout: page
---
Choqok is part of [KDE](https://kde.org) project, and source code is at [KDE official git repository](https://cgit.kde.org/choqok.git/).

It&#8217;s always under development and there are many features to add&#8230;

You can help its development via:

  * [coding](https://mail.kde.org/mailman/listinfo/choqok-devel)
  * [reporting bugs](https://bugs.kde.org/enter_bug.cgi?product=choqok)
  * [helping translation teams](https://l10n.kde.org/teams-list.php)
  * [making a donation](https://kde.org/donations)

## Help With Coding:

You can join [Choqok development mailing list](https://mail.kde.org/mailman/listinfo/choqok-devel) to have a discussion on development or other stuff with Choqok developers or people interested in its development.

Choqok's source code is hosted at KDE git repository, to have commit/push access you need to have a kde git account. [Check this](https://community.kde.org/Get_Involved) for more information.

**If you have any patches, please use [KDE Phabricator](https://phabricator.kde.org/)** and assign it to the [choqok project](https://phabricator.kde.org/project/profile/96/).

<p style="padding-left: 30px;">
  <em>For coding, I recommend <a href="https://community.kde.org/Policies/Kdelibs_Coding_Style">kdelibs coding style</a> for Choqok. Nobody is forced to use it. but to have consistent formatting of the source code files it is recommended to make use of it.</em>
</p>

## How To Extend Choqok ###

General information about Plugin development.

Any kind of plugins have to derived from [`Choqok::Plugin`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/plugin.h) class.

To create a plugin, you need to create a `.desktop` file which looks like that:

    [Desktop Entry]
    Encoding=UTF-8
    Type=Service
    X-Choqok-Version=1
    Icon=icon
    ServiceTypes=Choqok/Plugin
    X-KDE-Library=choqok_myplugin
    X-KDE-PluginInfo-Author=Your Name
    X-KDE-PluginInfo-Email=your@mail.com
    X-KDE-PluginInfo-Name=choqok_myplugin
    X-KDE-PluginInfo-Version=0.1
    X-KDE-PluginInfo-Website=http://yoursite.com
    X-KDE-PluginInfo-Category=Plugins
    X-KDE-PluginInfo-Depends=
    X-KDE-PluginInfo-License=GPL
    X-KDE-PluginInfo-EnabledByDefault=false
    Name=MyPlugin
    Comment=Plugin that do some nice stuff

The constructor of your plugin should looks like this:

```
K_PLUGIN_FACTORY_WITH_JSON(MyPluginFactory, "choqok_myplugin.json",
                           registerPlugin < MyPlugin > ();)

MyPlugin::MyPlugin(QObject *parent, const QList<QVariant> &args)
  : Choqok::Plugin(QLatin1String("choqok_myplugin"), parent)
{
//...
}
```

`Choqok::Plugin` inherits from [`KXMLGUIClient`](https://api.kde.org/frameworks/kxmlgui/html/classKXMLGUIClient.html). That client is added to the Choqok's mainwindow [`KXMLGUIFactory`](https://api.kde.org/frameworks/kxmlgui/html/classKXMLGUIFactory.html). So you may add actions on the main window.

Please note that the client is added right after the plugin is created, so you have to create every actions in the constructor.

### MicroBlog Plugin ###

A **MicroBlog Plugin** is an special plugin to support a microblogging service.
To create it, you have to create a General-Plugin that derived from [`Choqok::MicroBlog`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/microblog.h) class.

**Important functions to override:**

#### createNewAccount() ###

```
Choqok::Account *createNewAccount( const QString &alias )

    * @brief Create a new Account
    *
    * This method is called during the loading of the config file.
    * @param alias - the alias name to create the account with.
    *
    * you don't need to register the account to the AccountManager in this function.
    *
    * @return The new @ref Account object created by this function
```

Choqok Library has a general Account: [`Choqok::Account`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/account.h) implementation, but you can use a derived one, if needed.

#### createEditAccountWidget() ####

```
ChoqokEditAccountWidget *createEditAccountWidget( Choqok::Account *account, QWidget *parent )

    * @brief Create a new EditAccountWidget
    *
    * @return A new ChoqokEditAccountWidget to be shown in the account part of the configurations.
    *
    * @param account is the Account to edit. If it's 0L, then we create a new account
    * @param parent The parent of the 'to be returned' widget
```

EditAccountWidget is a [`QWidget`](https://doc.qt.io/qt-5/qwidget.html) that will use on Account adding and editing dialogs.

#### createMicroBlogWidget() ####

```
Choqok::UI::MicroBlogWidget * createMicroBlogWidget( Choqok::Account *account, QWidget *parent )

    * @brief Create a MicroBlogWidget for this Account
    * The returned MicroBlogWidget will show on Mainwindow. and manage of this microblog account will give to it
    * Every MicroBlog plugin should reimplement this.
    *
    * @return A new MicroBlogWidget to use.
    *
    * @param account account to use.
    * @param parent The parent of the 'to be returned' widget
```

Choqok Library has a general [`Choqok::UI::MicroBlogWidget`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/microblogwidget.h) implementation, but you can use a derived one, if needed.

#### createComposerWidget() ####

```
Choqok::UI::ComposerWidget * createComposerWidget( Choqok::Account *account, QWidget *parent )

    * @brief Create a ComposerWidget to use in MicroBlogWidget
    *
    * @return A new ComposerWidget to use.
    *
    * @param account account to use.
    * @param parent The parent of the 'to be returned' widget
```

Choqok Library has a general [`Choqok::UI::ComposerWidger`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/composerwidget.h) implementation, but you can use a derived one, if needed.

#### createTimelineWidget() ####

```
Choqok::UI::TimelineWidget * createTimelineWidget( Choqok::Account *account, const QString &timelineName, QWidget *parent )

    * @brief Create a TimelineWidget to use in MicroBlogWidget
    *
    * @return A new TimelineWidget to use.
    *
    * @param account account to use.
    * @param parent The parent of the 'to be returned' widget
```

Choqok Library has a general [`Choqok::UI::TimelineWidget`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/timelinewidget.h) implementation, but you can use a derived one, if needed.

#### createPostWidget() ####

```
Choqok::UI::PostWidget * createPostWidget( Choqok::Account *account, const Choqok::Post &post, QWidget *parent )

     * @brief Create a PostWidget to contain one post in TimelineWidget
     *
     * @return A new PostWidget to use.
     *
     * @param account account to use.
     * @param parent The parent of the 'to be returned' widget
```

Choqok Library has a general [`Choqok::UI::PostWidget`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/postwidget.h) implementation, but you can use a derived one, if needed.

Other useful functions to override:

#### createPost() ####

```
void createPost( Choqok::Account *theAccount, Choqok::Post *post )
```

To create a [`Choqok::Post`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/choqoktypes.h) on `theAccount` account.
SIGNAL `postCreated()` should emitted after successful creation.

#### abortCreatePost() ####

```
void abortCreatePost( Choqok::Account *theAccount, Choqok::Post *post = 0 )
```

To abort a post creation job, If `post` is 0, All post creation jobs should be aborted.

#### fetchPost() ####

```
void fetchPost( Choqok::Account *theAccount, Choqok::Post *post )
```

To fetch a special post, Needed functions about the post will provide with `post` var.
SIGNAL `postFetched()` should emitted after successful fetching with post.

#### removePost() ####

```
void removePost( Choqok::Account *theAccount, Choqok::Post *post )
```

To remove a special post, Needed functions about the post will provide with `post` var.
SIGNAL `postRemoved()` should emitted after successfully removed.

#### updateTimelines() ####

```
void updateTimelines( Choqok::Account *theAccount )
```

To update all timelines of `theAccount` account.
SIGNAL timelineDataReceived() will return new timelines.
And these are some of important functions and SIGNALs.

**Most important SIGNALs:**

#### timelineDataReceived() [SIGNAL] ####

```
void timelineDataReceived( Choqok::Account *theAccount, const QString &timelineName, QList<Choqok::Post*> data )
```

Should carry the data of timeline `timelineName` for account `theAccount`.

#### error() [SIGNAL] ####

```
void error( Choqok::Account *theAccount, Choqok::MicroBlog::ErrorType error, const QString &errorMessage, Choqok::MicroBlog::ErrorLevel level = Normal )
```

Should emitted after an error occurred, but there is one another SIGNAL for errors, that related to Posts, e.g. on post creation...

Data carried by this SIGNAL:

* `theAccount`: The account that this error occurred in.
* `error`: Type of error, it could be:
        /** A server side error. */
        ServerError,
        /** An error on communication with server */
        CommunicationError,
        /** A parsing error. */
        ParsingError,
        /** An error on authentication. */
        AuthenticationError,
        /** An error where the method called is not supported by this object. */
        NotSupportedError,
        /** Any other miscellaneous error. */
        OtherError
* `errorMessage`: The user readable error message.
* `level`: The priority level of problem, we have 3 level of errors: `Low`, `Normal`, `Critical`

When error level is `Low`, Just a window StatusMessage will show,
When error level is `Normal`, A system notification such as [`KNotification`](https://api.kde.org/frameworks/knotifications/html/classKNotification.html) will show.
And on `Critical` level a [`QMessageBox`](https://doc.qt.io/qt-5/qmessagebox.html) will showed to user.

### Shortner Plugin ###

A **Shortener Plugin** is an special plugin to use on [`Choqok::ShortenManager`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/shortenmanager.h) to shorten a link and derived from [`Choqok::Shortener`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/shortener.h) class.

A Shortener plugin have to provide URL shortening, So, Just one function is needed to override:

```
QString shorten( const QString &url )
```

We will provide a way for plugin configuration just like other plugins, By using a [`KCModule`](https://api.kde.org/frameworks/kconfigwidgets/html/classKCModule.html) plugin.

## DBus interface ##

### General Info ###

DBus Service: `org.kde.choqok`

DBus Object Path: `/`

DBus Interface: `org.kde.choqok`

### Methods ###

#### shareUrl() ####

```
void shareUrl(_Text_ url, _Bool_ title)
```

Will add the `url` to [`QuickPost`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/quickpost.h) widget and prepare it to post.
If `title` is true, Choqok will fetch the title of URL and add it to post.

#### uploadFile() ####

```
void uploadFile(_Text_ filename)
```

Upload medium dialog will shown with the `filename` to upload.

#### postText() ####

```
void postText(_Text_ post)
```

[`QuickPost`](https://lxr.kde.org/source/extragear/network/choqok/libchoqok/ui/quickpost.h) dialog will be ready to submit `post`.

#### updateTimelines() ####

```
void updateTimelines()
```

Update timeline, Just like the File -> Update Timelines action menu

#### setShortening() ####

```
void setShortening(_Bool_ enable)
```

Will set the _shortenOnPaste_ option.

#### getShortening() ####

```
_Bool_ getShortening()
```

Will get the _shortenOnPaste_ option.
