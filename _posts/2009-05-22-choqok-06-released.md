---
title: Choqok 0.6 released!
date: 2009-05-22T18:58:01+00:00
author: Mehrdad
layout: post
categories:
  - news
---
Choqok 0.6 codenamed &#8220;Strawberry&#8221; released, And is ready to use.<!--more-->

<!--split-->

[<img class="alignright" style="border: 0pt none;" src="/uploads/choqok-6.png?w=300" border="0" alt="Choqok 0.6" data-recalc-dims="1" />](/uploads/choqok-6.png)

Major improvements about new version is improved image caching backend, Prevent it to show an status more than once, And better support of showing right to left messages for all users. That fixes many bugs around them.

**Changes on this release:**

  * Add support for smilies in texts.
  * Add support for ReTweet function.
  * Twitpic.com integration added. With support of posting images to it and showing thumbnails of them.
  * Posting &#8220;_Currently listening to &#8230;_&#8221; text for Amarok 2.x users.
  * Option to use a custom browser instead of kde default implemented. (Requested by Gnome users)
  * Ability to abort posting new status.
  * A &#8220;Who is &#8230;&#8221; item added to user menu, to see Info, Homepage url and Name of a user.
  * Support for Digg.com shorturls service added.[<img class="alignright" title="Download" src="/uploads/download1.jpg?resize=68%2C80" border="0" alt="Download" data-recalc-dims="1" />](https://www.ohloh.net/p/choqok/download?filename=choqok-0.6.tar.bz2)
  * Better support for right to left languages, And users who use multiple languages with different directions.
  * Image caching improved and some related bugs fixed. (Daniel Schaal)

You can join ![Choqok](http://identi.ca/group/choqok "KDE micro-blogging client (choqok) group") group on [Identi.ca](http://identi.ca/) and be in touch with [me](http://identi.ca/mtux) and other users.

### Some other Choqok related news:

* Choqok source code is moved to KDE Extragear after previous release and you should use [KDE bugzilla](http://bugs.kde.org/) to report its bugs.

* Donation option is available to contribute to Choqok developments, just use the PayPal button at sidebar. Choqok can be even better.

* Sourceforge.net community choice award nomination is open until 29May, [Nominate Choqok for best new project](http://sourceforge.net/community/cca09/nominate/?project_name=Choqok&project_url=http://choqok.gnufolks.org/) if you enjoy it.