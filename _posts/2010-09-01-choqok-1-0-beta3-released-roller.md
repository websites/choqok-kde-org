---
title: 'Choqok 1.0 Beta3 released: &#8220;Roller&#8221;'
date: 2010-09-01T11:15:13+00:00
author: Mehrdad
layout: post
categories:
  - news
---
<p dir="ltr">
  We&#8217;re Happy to bring you a new Choqok version, a.k.a Choqok 1.0 Beta3 codenamed &#8220;<a href="http://en.wikipedia.org/wiki/Roller">Roller</a>&#8221; (The bird). The fact is Twitter shuts off Basic authentication, So current Choqok is used by more users, and We think about it as our Stable release, And A stable release needs more fixes than a beta.
</p>

<!--more-->

**There are so many fixes and changes around latest Choqok 1.0 Beta2 such as:**

  * Better nick name detection.
  * Better URL detection.
  * Fetching Friends List problems fixed.
  * UnTiny plugin can use untiny.com service instead of its generic way (Configurable)
  * Using the Choqok way to show repeated dents/tweets made optional (Default is Twitter way)
  * Confirm dialog for Re tweet
  * When KWallet isn&#8217;t available, Choqok will store secrets in text files (Not plain text, Base64)
  * Choqok 1.0Beta2 was needs kdelibs 4.5 , We changed this to work with 4.4 too.

And we have 2 new Url shortener plugins:

  * TinyArrows (By Andrey Esin)
  * Bit.ly (By Andrey Esin)

We were working on them in playground, and they are fully usable now.

Download source package from [here](https://www.ohloh.net/p/choqok/download?filename=choqok.0.9.90.tar.bz2). And follow install instruction in README.

**Important note for Twitter users**: You have to modify your account, and re-authenticate Choqok this time via OAuth.