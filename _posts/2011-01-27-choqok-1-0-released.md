---
title: Choqok 1.0 released
date: 2011-01-27T12:08:51+00:00
author: Mehrdad
layout: post
categories:
  - news
---
We are proud to finally announce release of Choqok 1.0 codenamed _Phoenix<!--more-->_

<p style="text-align: center;">
  <em><img title="Choqok 1.0" src="/uploads/splash_screen.png?resize=440%2C286" alt="Choqok 1.0" data-recalc-dims="1" /></em>
</p>

<p style="text-align: left;">
  You can download src package from <a href="http://choqok.gnufolks.org/pkgs/choqok-1.0.tar.bz2">here</a>.
</p>

<p style="text-align: left;">
  There are some bug fixes and improvements on <a href="http://choqok.gnufolks.org/2010/12/choqok-1-0-rc1-released-huma/">RC1 release</a>.
</p>

#### And some mentionable features instead 0.6.6 release are:

<p style="text-align: left;">
  &nbsp;
</p>

  * New and highly extendable design in code and UI
  * Support for &#8220;Open Collaboration Services Activities API&#8221; throught libAttica (It&#8217;s optional and you should have this library on build time)
  * Support for Twitpic, Flickr, Posterous, Mobypicture, Twitgoo, ImageShack (YFrog) image sharing services.
  * Several URL shortening services.
  * Preview a thumbnail of some Images and videos posted by others.
  * Share what you&#8217;re listening to with your friends with a wide range of player support
  * Set your dent/tweet as your current status on IM clients.
  * Filter out unwanted posts.
  * + some other simple but useful features that you can read about them in beta releases news.

We should thank all of you who helped us on this road, by testing new features and reporting bugs, or preparing patches for new features or bug fixes&#8230;

Cheers,
  
Choqok dev team.

&nbsp;