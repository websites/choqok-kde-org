---
title: Choqok 1.1 released
date: 2011-04-02T12:35:20+00:00
author: Mehrdad
layout: post
categories:
  - news
---
I&#8217;m happy to announce Choqok 1.1 release, codenamed &#8220;[Sizdah Be-dar](http://en.wikipedia.org/wiki/Sizdah_Be-dar)&#8220;.<!--more-->

[<img class="size-thumbnail wp-image-287 alignright" title="Choqok Translator" src="/uploads/choqok-translator-200x200.png?resize=200%2C200" alt="Choqok Translator Preview" srcset="/uploads/choqok-translator.png?resize=200%2C200 200w, /uploads/choqok-translator.png?resize=64%2C64 64w, /uploads/choqok-translator.png?zoom=2&resize=200%2C200 400w" sizes="(max-width: 200px) 100vw, 200px" data-recalc-dims="1" />](/uploads/choqok-translator.png)

#### It has two new plugins:

**Translator plugin**: for translating posts in other languages than our native one, (using Google translate service)

**Quick Filter plugin**: is a quick filter on choqok timeline, It&#8217;s developer &#8220;Farhad Hedayati Fard&#8221; explains:

> Assume you have 200 unread posts and you don&#8217;t have time to read all of them! you just want to read posts of your friend &#8220;geek&#8221; or posts that are
  
> about &#8220;April&#8217;s fools&#8221;! It&#8217;s difficult and time consuming to scroll down the
  
> timeline to find these posts&#8230;  you can use the &#8220;quick filter&#8221; plugin to apply filters on post author or post contents! simply select &#8220;Filter by content/author&#8221; in the tools menu and you&#8217;ll have a text box to enter your search term. Tada! you have all you want right after you hit enter!

#### **Other changes included:**

  * System tray icon problem in Gnome fixed.
  * Hide Menu-bar option.
  * Choqok caches posts automatically and not just at exit!
  * Annoying video preview plugin problem that causes Choqok to crash fixed!
  * Other Bug that fixed are: [243368](https://bugs.kde.org/show_bug.cgi?id=243368), [237265](https://bugs.kde.org/show_bug.cgi?id=237265), [265013](https://bugs.kde.org/show_bug.cgi?id=265013), [267852](https://bugs.kde.org/show_bug.cgi?id=267852), [268128](https://bugs.kde.org/show_bug.cgi?id=268128), [264255](https://bugs.kde.org/show_bug.cgi?id=264255), [267878](https://bugs.kde.org/show_bug.cgi?id=267878)

You can get the source code package of **Choqok 1.1** [here](http://sourceforge.net/projects/choqok/files/Choqok/choqok-1.1.tar.bz2/download), Or wait for distributions binary packages.

In addition to this release we have a _bug fix only_ release for Choqok 1.0 for stable distributions and whoever just need the bug fixes. that you can get it [here](http://sourceforge.net/projects/choqok/files/Choqok/choqok-1.0.1.tar.bz2/download). (**Choqok 1.0.1**)

__

<span style="color: #999999;">* Google is a trademark of <a href="http://google.com">Google</a> Inc.</span>